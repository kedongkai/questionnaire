﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using Questionnaire.Business.Infrastructure;
using Questionnaire.Business.Model;

namespace Questionnaire.Business.Repository
{
    public class DeveloperRepository
    {
        IMongoCollection<Developer> developerCollection;

        public DeveloperRepository()
        {
            var client = new MongoClient("mongodb+srv://admin:admin@cluster0-2193g.mongodb.net/"); // make a connection
            var database = client.GetDatabase("myprosperity"); // get a database

            var conventionPack = new ConventionPack()
            {
                new CamelCaseElementNameConvention(),
                //new IgnoreIfNullConvention(true),
                new IgnoreIfDefaultConvention(true),
                new IgnoreExtraElementsConvention(true)
            };
            ConventionRegistry.Register("conventions", conventionPack, t => true);

            developerCollection = database.GetCollection<Developer>("developer");
        }

        public void Insert(Developer developer)
        {
            developerCollection.InsertOne(developer);
        }

        public IEnumerable<Developer> GetAll()
        {
            var developers = developerCollection.AsQueryable().ToEnumerable();
            return developers;
        }
    }
}
