﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using Questionnaire.Business.Infrastructure;
using Questionnaire.Business.Model;

namespace Questionnaire.Business.Repository
{
    public class TemplateRepository
    {
        IMongoCollection<Template> templateCollection;

        public TemplateRepository()
        {
            var client = new MongoClient("mongodb+srv://admin:admin@cluster0-2193g.mongodb.net/"); // make a connection
            var database = client.GetDatabase("myprosperity"); // get a database

            var conventionPack = new ConventionPack()
            {
                new CamelCaseElementNameConvention(),
                //new IgnoreIfNullConvention(true),
                new IgnoreIfDefaultConvention(true),
                new IgnoreExtraElementsConvention(true)
            };
            ConventionRegistry.Register("conventions", conventionPack, t => true);

            templateCollection = database.GetCollection<Template>("template");
        }

        public void Insert(Template template)
        {
            templateCollection.InsertOne(template);
        }

        public IEnumerable<Template> GetAll()
        {
            var templates = templateCollection.AsQueryable().ToEnumerable();
            return templates;
        }

        public Template GetById(string id)
        {
            var template = templateCollection.AsQueryable<Template>().SingleOrDefault(x => x.Id == ObjectId.Parse(id));
            return template;
        }

        public void Update(Template template)
        {

        }
    }
}
