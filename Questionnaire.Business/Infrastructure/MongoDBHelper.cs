﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using System;
using System.Collections.Generic;

namespace Questionnaire.Business.Infrastructure
{
    public class MongoDBHelper
    {
        /*
        static IMongoCollection<BsonDocument> collection;
        static IMongoCollection<Developer> developerCollection;

        static void Init()
        {
            var client = new MongoClient("mongodb+srv://admin:admin@cluster0-2193g.mongodb.net/"); // make a connection
            var database = client.GetDatabase("myprosperity"); // get a database
            collection = database.GetCollection<BsonDocument>("template"); // get a collection

            // register the conventions
            var conventionPack = new ConventionPack()
            {
                new CamelCaseElementNameConvention(),
                //new IgnoreIfNullConvention(true),
                new IgnoreIfDefaultConvention(true),
                new IgnoreExtraElementsConvention(true)
            };
            ConventionRegistry.Register("conventions", conventionPack, t => true);

            // with mapping classes
            developerCollection = database.GetCollection<Developer>("developer");

            // crud
            var count = developerCollection.CountDocuments(new BsonDocument());
            Console.WriteLine($"developer count: " + count);

            var insertDeveloper = new Developer
            {
                Name = "D4",
                CompanyName = "WTG",
                KnowledgeBase = new List<Knowledge>() { new Knowledge { Language = "C#" }, new Knowledge { Language = "Java" } }
            };
            //developerCollection.InsertOne(insertDeveloper);

            var developers = developerCollection.AsQueryable().Where(x => x.CompanyName == "WTG");
            Console.WriteLine(developers.Count());
        }

        static void Count()
        {
            var count = collection.CountDocuments(new BsonDocument());
            Console.WriteLine($"count: " + count);
        }

        static void QueryAll()
        {
            var projection = Builders<BsonDocument>.Projection.Exclude("_id");
            var documents = collection.Find(new BsonDocument()).Project(projection).ToList();
            foreach (var document in documents)
            {
                Console.WriteLine(document.ToString());
            }
        }

        static void Query()
        {
            var filter = Builders<BsonDocument>.Filter.Eq("counter", 71);
            var document = collection.Find(filter).FirstOrDefault();
            Console.WriteLine(document);
        }

        static void QueryRange()
        {
            var filterBuilder = Builders<BsonDocument>.Filter;
            var filter = filterBuilder.Gt("counter", 50) & filterBuilder.Lte("counter", 60);
            var cursor = collection.Find(filter).ToCursor();
            foreach (var document in cursor.ToEnumerable())
            {
                Console.WriteLine(document);
            }
        }

        static void Insert()
        {
            var document = new BsonDocument
            {
                { "name", "MongoDB" },
                { "type", "Database" },
                { "count", 1 },
                { "info", new BsonDocument
                    {
                        { "x", 203 },
                        { "y", 102 }
                    }}
            };

            collection.InsertOne(document);
        }

        static void InsertMulti()
        {
            var documents = Enumerable.Range(0, 100).Select(i => new BsonDocument("counter", i));
            collection.InsertMany(documents);
        }

        static void Update()
        {
            var filter = Builders<BsonDocument>.Filter.Eq("counter", 10);
            var update = Builders<BsonDocument>.Update.Set("counter", 110);
            var result = collection.UpdateOne(filter, update);
            if (result.IsModifiedCountAvailable)
            {
                Console.WriteLine($"updated: " + result.ModifiedCount);
            }
        }

        static void Delete()
        {
            var filter = Builders<BsonDocument>.Filter.Eq("counter", 110);
            var result = collection.DeleteOne(filter);
            if (result.DeletedCount > 0)
            {
                Console.WriteLine($"deleted: " + result.DeletedCount);
            }
        }
        */
    }
}
