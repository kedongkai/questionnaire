﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace Questionnaire.Business.Model
{
    public class Developer
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public string CompanyName { get; set; }
        public List<Knowledge> KnowledgeBase { get; set; }
    }

    public class Knowledge
    {
        public string Language { get; set; }
        public string Technology { get; set; }
        public short Rating { get; set; }
    }
}
