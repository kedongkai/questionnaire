﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace Questionnaire.Business.Model
{
    public class Template
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public IEnumerable<Question> Questions { get; set; }
    }

    public class Question
    {
        public string Type { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }
}
