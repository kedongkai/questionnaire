﻿using Questionnaire.Business.Model;
using System.Collections.Generic;

namespace Questionnaire.Models
{
    public class DeveloperViewModel
    {
        public IEnumerable<Developer> Developers { get; set; }
    }
}
