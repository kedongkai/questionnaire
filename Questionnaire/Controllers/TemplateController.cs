﻿using Microsoft.AspNetCore.Mvc;
using Questionnaire.Business.Repository;
using Questionnaire.Models;
using System.Linq;

namespace Questionnaire.Controllers
{
    public class TemplateController : Controller
    {
        public IActionResult Index()
        {
            var repo = new TemplateRepository();
            var templates = repo.GetAll();
            return View(templates);
        }

        public IActionResult Details(string id)
        {
            var repo = new TemplateRepository();
            var template = repo.GetById(id);
            return View(template);
        }

        public IActionResult Edit(string id)
        {
            var repo = new TemplateRepository();
            var template = repo.GetById(id);
            return View(template);
        }
    }
}
