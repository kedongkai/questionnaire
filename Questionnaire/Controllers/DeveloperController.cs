﻿using Microsoft.AspNetCore.Mvc;
using Questionnaire.Business.Repository;
using Questionnaire.Models;

namespace Questionnaire.Controllers
{
    public class DeveloperController : Controller
    {
        public IActionResult Index()
        {
            DeveloperRepository repo = new DeveloperRepository();
            var developers = repo.GetAll();
            var viewModel = new DeveloperViewModel() { Developers = developers };
            return View(viewModel);
        }
    }
}
